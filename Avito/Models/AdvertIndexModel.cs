﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avito.Models
{
    public class AdvertIndexModel
    {
        public IList<AdvertModel> Adverts { get; set; }
        public int? Page { get; set; }
    }
}
