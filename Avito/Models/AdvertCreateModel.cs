﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Avito.Models
{
    public class AdvertCreateModel
    {
        [Required]
        [Display(Name = "Заглавие")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public Decimal Price { get; set; }
    }
}
