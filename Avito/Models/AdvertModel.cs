﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Avito.Models
{
    public class AdvertModel
    {
        public int Id { get; set; }
        
        [Display(Name = "Дата публикации")]
        public string DateCreated { get; set; }

        [Display(Name = "Заглавие")]
        public string Title { get; set; }
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
    }
}
