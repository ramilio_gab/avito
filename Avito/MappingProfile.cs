﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Avito.DAL.Entities;
using Avito.Models;

namespace Avito
{
    public class MappingProfile : Profile

    {
        public MappingProfile()
        {
        }

        public void CreateAdvertToAdvertModelMap()
        {
            CreateMap<Advert, AdvertModel>()
                .ForMember(target => target.DateCreated,
                    src => src.MapFrom(p => p.DateCreated.ToString("D")))
                .ForMember(target => target.Title,
                    src => src.MapFrom(p => p.Title));
        }
    }
}
