﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avito.DAL.Repositories.Contracts;
using Avito.Services.Contracts;

namespace Avito.Services
{
    public class AdvertService : IAdvertService

    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdvertService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            
        }
    }
}
