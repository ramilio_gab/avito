﻿using System;
using System.Threading.Tasks;
using Avito.DAL.Entities;
using Avito.Models;
using Avito.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Avito.Controllers
{
    public class AdvertController : Controller
    {
        private readonly IAdvertService _advertService;
        private readonly UserManager<User> _userManager;

        public AdvertController(IAdvertService advertService, UserManager<User> userManager)
        {
            _advertService = advertService;
            _userManager = userManager;
        }

        public IActionResult Index(AdvertIndexModel model)
        {
            try
            {
                var advertModels = _advertService.GetAllAdverts(model);
                model.Adverts = advertModels;
                return View();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize]
        [HttpPost]

        public async Task<IActionResult> CreateTopic(AdvertCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _topicService.CreateTopic(model, currentUser.Id);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

}
