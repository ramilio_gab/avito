﻿using Avito.DAL.Entities;
using Avito.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Avito.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>

    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;
        public DbSet<Advert> Adverts { get; set; }

        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity(_entityConfigurationsContainer.AdvertConfiguration.ProvideConfigurationAction());
        }
    }
}