﻿using Avito.DAL.Entities;

namespace Avito.DAL.EntitiesConfiguration.Contracts
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer

    {
        public IEntityConfiguration<Advert> AdvertConfiguration { get; }
        public EntityConfigurationsContainer()
        {
            
            AdvertConfiguration = new AdvertConfiguration();
        }


        
    }
}
