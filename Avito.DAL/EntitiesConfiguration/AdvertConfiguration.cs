﻿using System;
using System.Collections.Generic;
using System.Text;
using Avito.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Avito.DAL.EntitiesConfiguration
{
    public class AdvertConfiguration : BaseEntityConfiguration<Advert>

    {
        protected override void ConfigureProperties(EntityTypeBuilder<Advert> builder)
        {
            builder
                .Property(b => b.Title)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Contacts)
                .IsRequired();
            builder
                .Property(b => b.DateCreated)
                .IsRequired();
            builder
                .Property(b => b.Description)
                .HasMaxLength(Int32.MaxValue)
                .IsRequired();
        }
    }
}
