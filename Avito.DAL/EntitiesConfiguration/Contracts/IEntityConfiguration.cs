﻿using System;
using System.Collections.Generic;
using System.Text;
using Avito.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Avito.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    { 
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
