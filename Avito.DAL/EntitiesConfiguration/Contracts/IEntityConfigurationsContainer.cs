﻿using Avito.DAL.Entities;

namespace Avito.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Advert> AdvertConfiguration { get; }
    }
}