﻿using System;
using Avito.DAL.Repositories.Contracts;

namespace Avito.DAL.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;
        public IAdvertRepository Adverts { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Adverts = new AdvertRepository(context);
        }



        

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
