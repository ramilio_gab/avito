﻿using Avito.DAL.Entities;
using Avito.DAL.Repositories.Contracts;

namespace Avito.DAL.Repositories
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory

    {
        private readonly IApplicationDbContextFactory _applicationDbContextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory applicationDbContextFactory)
        {
            _applicationDbContextFactory = applicationDbContextFactory;
        }

        public UnitOfWork Create()
        {
            return new UnitOfWork(_applicationDbContextFactory.Create());
        }
    }
}
