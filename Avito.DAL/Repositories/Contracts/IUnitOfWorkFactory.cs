﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avito.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
