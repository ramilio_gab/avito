﻿using System;
using System.Collections.Generic;
using System.Text;
using Avito.DAL.Entities;
using Avito.DAL.Repositories.Contracts;

namespace Avito.DAL.Repositories
{
    public class AdvertRepository : Repository<Advert>, IAdvertRepository
    {
        public AdvertRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Adverts;
        }
       
    }

}
