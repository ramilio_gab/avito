﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avito.DAL.Entities
{
    public class Advert : IEntity
    {
        public Advert(int categoryId, decimal price)
        {
            CategoryId = categoryId;
            Price = price;
        }

        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public string Contacts { get; set; }

    }
}
