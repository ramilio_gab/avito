﻿using Microsoft.AspNetCore.Identity;

namespace Avito.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
