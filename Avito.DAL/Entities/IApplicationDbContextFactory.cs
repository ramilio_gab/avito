﻿namespace Avito.DAL.Entities
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
