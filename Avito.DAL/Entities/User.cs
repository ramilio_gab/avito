﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Avito.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    { 
        public ICollection<Advert> Adverts { get; set; }
    }
}